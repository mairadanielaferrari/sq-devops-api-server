<persist>true</persist>
<powershell>
$bucketName = "${s3-bucket-name}"
$rootPath = "C:\\Squirt\\"
$scriptPath ="$rootPath\\Scripts\"
$settingsFileName = "settings.json"
$webConfigPath = "$rootPath\\Api\\web.config"
$sqlServer = ""
$sqlDbName = "SquirtPrimary"
$sqlUid = ""
$sqlPwd = ""
$url = (new-object System.Net.WebClient).DownloadString("http://169.254.169.254/latest/meta-data/public-hostname")
$url = "http://$url"

Copy-S3Object -BucketName $bucketName -key $settingsFileName -file $scriptPath\$settingsFileName

$settingsJson = Get-Content("$scriptPath\$settingsFileName") | Out-String | ConvertFrom-Json
$sqlServer = $settingsJson."sql-server-dns"
$sqlDbName = "SquirtPrimary"
$sqlUid = $settingsJson."sql-api-username"
$sqlPwd = $settingsJson."sql-api-password"

$redisAuthToken=$settingsJson."redis-server-password"
$redisAddress=$settingsJson."redis-server-address"
$redisPort=$settingsJson."redis-server-port"
$redisConnectionString="$redisAddress" + ":" + "$redisPort" + ",password=$redisAuthToken"

$adoConnectionString = "Provider=SQLOLEDB;Data Source=$sqlServer;Initial Catalog=$sqlDbName;User ID=$sqlUid; Password=$sqlPwd;"
$dotNetConnectionString = "Server=$sqlServer;Database=$sqlDbName;User ID=$sqlUid;Password=$sqlPwd;"

###########################################################################################################################
# Update registry
###########################################################################################################################
$registryPath='HKEY_LOCAL_MACHINE\SOFTWARE\Squirt35'

New-ItemProperty -Path Registry::$registryPath -Name "ConnectionStringPrimary" -Value $adoConnectionString -Force
New-ItemProperty -Path Registry::$registryPath -Name "NETConnectionStringPrimary" -Value $dotNetConnectionString -Force
New-ItemProperty -Path Registry::$registryPath -Name "RedisSessionConnectionString" -Value $redisConnectionString -Force

$redisAuthToken
###########################################################################################################################

###########################################################################################################################
# Update web.config
###########################################################################################################################
$dotNetConnectionString = "Server=$sqlServer;Database=$sqlDbName;User ID=$sqlUid;Password=$sqlPwd;"

[xml]$webConfigXml = Get-Content -Path $webConfigPath

$key = "webApi:redisServer"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisAddress"

$key = "webApi:redisServerPort"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisPort"

$key = "webApi:redisServerPassword"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisAuthToken"

$key = "WebApiRateLimiter:RedisConnString"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisConnectionString,abortConnect=false"

$webConfigXml.Save($webConfigPath)
###########################################################################################################################
</powershell>