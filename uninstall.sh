#!/bin/bash

export PATH=$PATH:/opt/terraform &&
terraform init &&

# Argument/Env Variable Verification
if [ \( -z "$AWS_ACCESS_KEY_ID" \) -a \( $# -ne 4 \) ]
  then
    echo -e "Invalid number of arguments/environment variables \n
		1) AWS_ACCESS_KEY_ID \n
		2) AWS_SECRET_ACCESS_KEY \n
		3) AWS_DEFAULT_REGION \n
		4) BUCKET_NAME \n
	  Example: ./install.sh ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY} ${AWS_DEFATUL_REGION} ${BUCKET_NAME} \n"
    exit
elif [ $# -eq 4 ]
	then
		export AWS_ACCESS_KEY_ID=$1
		export AWS_SECRET_ACCESS_KEY=$2
		export AWS_DEFAULT_REGION=$3
		export BUCKET_NAME=$4
fi

echo "Downloading terraform state. Bucket Name $BUCKET_NAME"
aws s3 cp s3://$BUCKET_NAME/api/terraform.tfstate . &&

echo "Downloading settings.json"
aws s3 cp s3://$BUCKET_NAME/settings.json . &&

echo "Executing Terraform..."

terraform destroy \
  -var "aws-access-key=$1" \
  -var "aws-secret-key=$2" \
  -var "aws-region=$3" \
	-var "s3-bucket-name=$BUCKET_NAME" \
  -var-file="settings.json" \
  -auto-approve

rm -f ./settings.json