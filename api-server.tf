locals {
  public-subnets                = "${var.subnets["public"]}"
}

data "template_file" "user-data" {
  template = "${file("./user-data.ps1")}"
  vars {
    s3-bucket-name = "${var.s3-bucket-name}"
  }
}

resource "aws_instance" "api-server" {
  count                         = "${var.api-server-instance-count}"
  ami                           = "${var.api-server-ami-id}"
  instance_type                 = "${var.api-server-instance-type}"
  key_name                      = "${var.api-server-key-pair}"
  subnet_id                     = "${element(local.public-subnets, 0)}"
  associate_public_ip_address   = true
  security_groups               = ["${var.web-server-sg-id}"]
  iam_instance_profile          = "${var.ec2-instance-profile-name}"
  user_data                     = "${data.template_file.user-data.rendered}"
  
  tags {
    Name            = "${var.deployment-prefix}-api-server"
    Organization    = "${var.organization}"
    Project         = "${var.project}"
    Environment     = "${var.environment}"
  }
}